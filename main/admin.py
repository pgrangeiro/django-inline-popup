 # -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.contenttypes import generic
from models import *


class DocumentoInline(generic.GenericStackedInline):
    model = Documento
    max_num = 1


class AdicaoInline(generic.GenericTabularInline):
    model = Adicao
    extra = 0


class ItemInline(admin.TabularInline):
    model = Item

class AdicaoAdmin(admin.ModelAdmin):
    model = Adicao
    inlines = (ItemInline,)


class MovimentoAdmin(admin.ModelAdmin):
    model = Movimento
    inlines = (DocumentoInline, AdicaoInline)


admin.site.register(Movimento, MovimentoAdmin)
admin.site.register(Adicao, AdicaoAdmin)
admin.site.register(Documento)
