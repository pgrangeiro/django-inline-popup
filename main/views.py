from models import *
from django.http import HttpResponse
import json


def get_adicao(request, id):
    data = {}
    try:
        adicao = Adicao.objects.get(id=id)
        data[adicao.id] = {'numero': adicao.numero, 'documento': adicao.documento.id}
    except Adicao.DoesNotExist:
        pass

    return HttpResponse(json.dumps(data), mimetype='text/javascript')
