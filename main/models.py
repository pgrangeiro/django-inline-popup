# -*- coding: utf-8 -*- 
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic


class Documento(models.Model):
    numero = models.CharField(u'Número', max_length=10)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    def __unicode__(self):
        return 'Documento %s' % self.numero


class Adicao(models.Model):
    numero = models.IntegerField(u'Número')
    documento = models.ForeignKey(Documento, verbose_name=u'Documento')
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    def __unicode__(self):
        return u'Adição %s' % self.numero


class Item(models.Model):
    numero = models.IntegerField(u'Número')
    adicao = models.ForeignKey(Adicao, verbose_name=u'Adição')
    valor = models.DecimalField(u'Valor', decimal_places=2, max_digits=5)

    def __unicode__(self):
        return u'Item %s' % self.numero


class Movimento(models.Model):
    documento = models.ForeignKey(Documento, verbose_name=u'Documento')
    data = models.DateTimeField(u'Data')

    def __unicode__(self):
        return self.id
